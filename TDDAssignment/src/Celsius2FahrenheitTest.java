import static org.junit.Assert.*;

import org.junit.Test;

public class Celsius2FahrenheitTest{

	//Test #1
	//Test Objective : Convert Celsius to Fahrenheit
	//Test Inputs: Celsius = 0
	//Test Expected Output: Fahrenheit = 32
	
	
	@Test public void testtoFahrenheit001() {
		assertEquals(32,Celsius2Fahrenheit.toFahrenheit(0),0.001); //epsilon (fuzz factor) difference between doubles
	}
	//Test #2
	//Test Objective : Convert Fahrenheit to Celsius
	//Test Inputs: Fahrenheit = 32
	//Test Expected Output: Celsius = 0
		
		
	@Test public void testtoCelsius002() {
	
	assertEquals(0,Celsius2Fahrenheit.toCelsius(32),0.001);
	
	}
	
	//Test #3
	//Test Objective : Convert Fahrenheit to Celsius
	//Test Inputs: Fahrenheit = -500
	//Test Expected Output: Celsius = "Enter a correct Fahrenheit Value (0 - 212°)"
			
			
	@Test public void testErrorCheck003() {
		
	assertEquals("Enter a correct Fahrenheit Value (−459.67 to 212°)",Celsius2Fahrenheit.toCelsius(-500));
	}
	
	//Test #4
		//Test Objective : Convert Fahrenheit to Celsius
		//Test Inputs: Celsius = -500
		//Test Expected Output: Fahrenheit = "Enter a correct Celsius Value (0 - 212°)"
				
				
		@Test public void testErrorCheck004() {
			
		assertEquals("Enter a correct Celsius Value (−273.15 to 100°)",Celsius2Fahrenheit.toFahrenheit(-500));
	
	}
		
		//Test #5
		//Test Objective : Convert Celsius to Fahrenheit
		//Test Inputs: Celsius = -273.15
		//Test Expected Output: Fahrenheit = -459.67
		
		
		@Test public void testtoFahrenheit005() {
			assertEquals(-459.66999999999996,Celsius2Fahrenheit.toFahrenheit(-273.15),0.001); //epsilon (fuzz factor) difference between doubles
		}
		
		
		//Test #6
		//Test Objective : Convert Fahrenheit to Celsius
		//Test Inputs: Fahrenheit = -459.17
		//Test Expected Output: Celsius = -273.15
			
			
		@Test public void testtoCelsius006() {
		
		assertEquals(-273.15,Celsius2Fahrenheit.toCelsius(-459.67),0.01);
		
		}

}
