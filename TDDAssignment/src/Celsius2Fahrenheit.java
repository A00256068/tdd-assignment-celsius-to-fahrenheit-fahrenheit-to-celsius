import java.util.InputMismatchException;
import  java.util.Scanner;


public class Celsius2Fahrenheit {



	public static double toFahrenheit(double c) {

		c = 32 + ( 9 * c / 5);
		 return c;

	}
	
	
	public static double toCelsius(double f) {
		
		f = 5 *(f - 32.0) / 9.0;
		 return f;
		 
	}

	public static void main(String[] args) {

		
		
		
		    
		System.out.println("Celsius to Fahrenheit example");
		System.out.println("Please enter a temperature in Degrees Celsius:\n");
		
		Scanner in = new Scanner(System.in);
		
		double c = in.nextDouble(); //celsius temp 
		if(c>=-273.15 && c<= 100)
		{
		double f = toFahrenheit(c); // fahrenheit conversion
		System.out.println(c + "°C converts to " +(f) + "°F");
		System.out.println();
		}
		else
		{
			System.out.println("Enter a correct Celsius Value (−273.15 to 100°)");
			double errorC = in.nextDouble(); //celsius temp 
			double errorF = toFahrenheit(errorC); // fahrenheit conversion
			System.out.println(errorC + "°C converts to " +(errorF) + "°F");
			System.out.println();
		}
		
	
		
		
		
		System.out.println("Please enter a temperature in Degrees Fahrenheit:\n");
	
		double f1 = in.nextDouble(); //fahrenheit temp 
		if(f1>=-459.67 && f1<= 212)
		{
		double c1 = toCelsius(f1); // celsius conversion
		System.out.println(f1 + "°F converts to " +(c1) + "°C");
		System.out.println();
		}
		else
		{
			System.out.println("Enter a correct Fahrenheit Value (−459.67 to 212°)");
			double errorF = in.nextDouble(); //fahrenheit temp 
			double errorC = toCelsius(errorF); // celsius conversion
			System.out.println(errorF + "°F converts to " +(errorC) + "°C");
			System.out.println();
			
		}
		

	
		   }
	}


